var Main = {}; //construimos el objeto main que va a a ejecutar todas las instrucciones
var player; // declaramos el objeto player que recibe el rectangulo donde se va a reproducir el video
var cnt = 0;
var flag = false;


var tizenId = tizen.application.getCurrentApplication().appInfo.packageId;//obteneemos el id del monitor
var url = "file:///opt/usr/apps/"+tizenId +"/res/wgt/media/walmart.mp4"; //la direccion donde esta el video en la memoria del monitor

//Esta es la funcion que ejecuta al inicar la aplicacion con ella definimos si el dispositivo tiene tizen o no, 
//si tiene tizen empieza entonces a ejecutar las funciones que vamos a crear 
Main.onLoad = function (){
    "use strict";
     if (window.tizen === undefined) {
        
         setTimeout(
                function(){
                     console.log('This application needs to be run on Tizen device');
                },
                1000
         );
     }else{
        setTimeout(function () {
            console.log("La aplicacion se ha iniciado");
            Main.initplayer();// se debe iniciar el player
            document.addEventListener('click', Main.onTouch, true);  
        }, 500)         
     }

     window.onhide = function(){
         wepapis.avplay.close();
         
     }
};

Main.onTouch = function () {
        document.getElementById("av-player").addEventListener('click', function (e) {        
            console.log("Me presionaron");
            webapis.avplay.stop();
            $("#av-player").remove();
            flag = true;
            setInterval(Main.itera, 1000);          
        });
};

Main.itera = function () {
    document.getElementById("body").addEventListener('click', function (e) {
        console.log('Contador reiniciado');
        cnt = 0;
    });
    cnt++;
    console.log(cnt);

    if ((cnt > 15) && (flag)) {
        flag = false;
        console.log('Ya debo morir');
        location.reload();
    }
}



Main.initplayer = function(){
    "use strict";
    console.log("Player iniciado");
    // DOM reference to avplay object
    player = document.getElementById('av-player');
    Main.play(url);
};

//start playback
Main.play = function(url) {
    "use strict";
    // Esta estructura de listener se copia y pega tal cual en el codigo asi como viene en la pagina
    var listener = {
            onbufferingstart: function() {
                console.log("Buffering start.");
            },
            onbufferingprogress: function(percent) {
                console.log("Buffering progress percent : " + percent);
            },
            onbufferingcomplete: function() {
                console.log("Buffering complete.");
            },
            oncurrentplaytime: function(currentTime) {
                console.log("Current Playtime : " + currentTime);
            },
            onevent: function(eventType, eventData) {
                console.log("event type error : " + eventType + ", data: " + eventData);
            },
            onerror: function(eventType) {
                console.log("event type error : " + eventType);
            },
            onstreamcompleted: function() {
                console.log("Stream Completed");
                webapis.avplay.stop();
                Main.play(url);
            }
        };
        
        
    
    console.log('open: '+url);
    console.log('open status: '+ webapis.avplay.open(url));
    console.log('setListener status: '+ webapis.avplay.setListener(listener));
    console.log('prepare status: '+ webapis.avplay.prepare());
    

    webapis.avplay.setDisplayRect(0, 0, 1920, 1080); //DEfinimos el tamaño del video o lo recortamos
    //webapis.avplay.setDisplayRotation("PLAYER_DISPLAY_ROTATION_90") // ajustamos la rotacion del video
    console.log('play status: '+webapis.avplay.play());
    
};

Main.suspend = function(){
    webapis.avplay.close();
}

//stop playback
Main.stop = function() {
    "use strict";
    webapis.avplay.stop();

};
