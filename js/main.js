var cnt = 0;
var flag = false;

//Esta es la funcion que ejecuta al inicar la aplicacion con ella definimos si el dispositivo tiene tizen o no, 
//si tiene tizen empieza entonces a ejecutar las funciones que vamos a crear 
Main.onLoad = function (){
	"use strict";
	 if (window.tizen === undefined) {
        
         setTimeout(
         		function(){
         			 console.log('This application needs to be run on Tizen device');
         		},
         		1000
         );
     }else{
     	setTimeout(function () {
     		console.log("La aplicacion se ha iniciado");
     		Main.initplayer();// se debe iniciar el player
         	document.addEventListener('click', Main.onTouch, true);  
     	}, 500)         
     }

     window.onhide = function(){
         wepapis.avplay.close();
         
     }
};


Main.onTouch = function () {
		document.getElementById("av-player").addEventListener('click', function (e) {        
	    	console.log("Me presionaron");
	    	webapis.avplay.stop();
	    	$("#av-player").remove();
	    	flag = true;
	    	setInterval(Main.itera, 1000);	    	
	    });
};

Main.itera = function () {
	document.getElementById("body").addEventListener('click', function (e) {
		console.log('Contador reiniciado');
		cnt = 0;
	});
	cnt++;
	console.log(cnt);

	if ((cnt > 15) && (flag)) {
		flag = false;
		console.log('Ya debo morir');
		location.reload();
	}
}

